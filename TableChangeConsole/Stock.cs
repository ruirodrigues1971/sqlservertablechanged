﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TableChangeConsole
{
    public class Stock
    {
        public decimal Price { get; set; }
        public string Symbol { get; set; }
        public string Name { get; set; }
        override
            public string ToString()
        {
            return $"{Name}:{Symbol}:{Price}";
        }
    }
}
