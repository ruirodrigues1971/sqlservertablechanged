﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using TableDependency;
using TableDependency.SqlClient;
using TableDependency.SqlClient.Base.EventArgs;
using TableDependency.SqlClient.Base;
using TableDependency.SqlClient.Base.Enums;


//https://github.com/christiandelbianco/monitor-table-change-with-sqltabledependency
//https://dzone.com/articles/receive-notifications-with-new-values-when-table-r

namespace TableChangeConsole
{
    /* Não esquecer
     * ALTER DATABASE TesteTableChanged SET ENABLE_BROKER WITH ROLLBACK IMMEDIATE;
     * SELECT is_broker_enabled FROM sys.databases WHERE name = 'TesteTableChanged';
     * 
     * Service SQLBrowser tem de estar a correr
     */

    /// <summary>
    /// 
    /// </summary>
    class Program
    {
        private IList<Stock> _stocks;
        const string schemaName = "TesteTableChanged";
        private static readonly string _connectionString = $"Data Source=RUI-R;Initial Catalog={schemaName};Integrated Security=True";
        private readonly SqlTableDependency<Stock> _dependency;
        
        Program()
        {
            _stocks = new List<Stock>();
            AppDomain.CurrentDomain.ProcessExit += new EventHandler(CurrentDomain_ProcessExit);
            var mapper = new ModelToTableMapper<Stock>();
            mapper.AddMapping(model => model.Symbol, "Code");
            const string tableName = "Stocks";
            
            _dependency = new SqlTableDependency<Stock>(_connectionString, tableName, mapper:mapper);
            _dependency.OnChanged += Dependency_OnChanged;
            _dependency.OnError += Dependency_OnError;
            _dependency.Start();
        }

        private void CurrentDomain_ProcessExit(object sender, EventArgs e)
        {
            _dependency.Stop();
        }

        private void Dependency_OnError(object sender, ErrorEventArgs e)
        {
            throw e.Error;
        }

        private void Dependency_OnChanged(object sender, RecordChangedEventArgs<Stock> e)
        {
            if (_stocks != null)
            {
                if (e.ChangeType != ChangeType.None)
                {
                    switch (e.ChangeType)
                    {
                        case ChangeType.Delete:
                            _stocks.Remove(_stocks.FirstOrDefault(c => c.Symbol == e.Entity.Symbol));
                            Console.WriteLine($"Deleted:{e.Entity}");
                            break;
                        case ChangeType.Insert:
                            _stocks.Add(e.Entity);
                            Console.WriteLine($"Inserted:{e.Entity}");
                            break;
                        case ChangeType.Update:
                            var customerIndex = _stocks.IndexOf(_stocks.FirstOrDefault(c => c.Symbol == e.Entity.Symbol));
                            if (customerIndex >= 0) _stocks[customerIndex] = e.Entity;
                            Console.WriteLine($"Update:{e.Entity}");
                            break;
                    }
                    
                }
            }
        }


        static void Main(string[] args)
        {
            if (TesteConnection())
            {
                Console.WriteLine("Data base connected!");
                new Program();
            } else
            {
                Console.WriteLine("Data base not connected!");
            }
            Console.WriteLine("Press Enter to close");
            Console.ReadLine();

        }

        private static bool TesteConnection()
        {
            using (SqlConnection connection = new SqlConnection(_connectionString))
            {
                try
                {
                    //var query = $"ALTER DATABASE {schemaName} SET ENABLE_BROKER";
                    var query = "Select 1";
                    var command = new SqlCommand(query, connection);
                    connection.Open();
                    var result = command.ExecuteScalar();

                    return true;
                }
                catch (SqlException e)
                {
                    return false;
                }
            }
        }
    }
}
